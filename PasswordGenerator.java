import java.util.Random;

public class PasswordGenerator {
    private int passwordLength;
    private static final String WEAK_CHARSET;
    private static final String STRONG_CHARSET;
    private static final Random RND;
    private static final StringBuilder BUILDER;

    static {
        WEAK_CHARSET = "abcdefghijklmnopqrstuvwxyz";
        STRONG_CHARSET = "ABCDEFGHIJKLMOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        RND = new Random();
        BUILDER = new StringBuilder();
    }

    public PasswordGenerator(int passwordLength) {
        this.passwordLength = passwordLength;
    }

    public String generateWeakPassword() {
        BUILDER.setLength(0);
        
        for (int i = 0; i < passwordLength; i++) {
            int letterIndex = RND.nextInt(WEAK_CHARSET.length());
            BUILDER.append(WEAK_CHARSET.charAt(letterIndex));
        }

        return BUILDER.toString();
    }

    public String generateStrongPassword() {
        BUILDER.setLength(0);
        
        for (int i = 0; i < passwordLength * 2; i++) {
            int letterIndex = RND.nextInt(WEAK_CHARSET.length());
            BUILDER.append(STRONG_CHARSET.charAt(letterIndex));
        }

        return BUILDER.toString();
    }
}
