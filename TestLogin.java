import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestLogin {
    @Test
    public void testRegular() {
        User[] users = {
            new User("User", "passwd", "123456789", UserType.REGULAR),
            new User("AAAA", "testpassword", "987654321", UserType.REGULAR),
        };

        Login login = new Login(users);
        assertEquals(true, login.validate("User", "passwd", false));
        assertEquals(true, login.validate("AAAA", "testpassword", false));
        assertEquals(false, login.validate("User", "password", false));
        assertEquals(false, login.validate("AAAAAAAA", "testpassword", true));
    }
    
    @Test
    public void testAdmin() {
        User[] users = {
            new User("User", "passwd", "123456789", UserType.REGULAR),
            new User("Pablo", "admin", "000000000", UserType.ADMIN),
        };

        Login login = new Login(users);
        assertEquals(true, login.validate("Pablo", "admin", true));
        assertEquals(false, login.validate("Pablo", "admin123", true));
    }
}
