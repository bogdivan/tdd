public class User{
    private String username;
    private String password;
    private String phoneNum;
    private UserType user;

    public User (String username, String password, String phoneNum, UserType user){
        this.username = username;
        this.password = password;
        this.phoneNum = phoneNum;
        this.user = user;
    }

    public UserType getUser() {
        return this.user;
    }
    public String getUsername() {
        return this.username;
    }
    public String getPhoneNum() {
        return this.phoneNum;
    }
    public String getPassword() {
        return this.password;
    }
}