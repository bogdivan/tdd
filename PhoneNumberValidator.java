import java.util.regex.Pattern;

public class PhoneNumberValidator {
    private boolean bracketsAllowed;
    private User phone;

    public PhoneNumberValidator(boolean bracketsAllowed){
        this.bracketsAllowed = bracketsAllowed;        
    }

    public boolean validate(String number){
        //final Pattern pattern = Pattern.compile("^[0-9]");
        if(bracketsAllowed){
        
            if (Pattern.matches("^[0-9()]*$", number)){
                return true;
            }
            else{
                return false;
            }
        }
        else {
            if (Pattern.matches("^[0-9]*$", number)){
                return true;
            }
            else {
                return false;
            }
        }
 
    }
}
