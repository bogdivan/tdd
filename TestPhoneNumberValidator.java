import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.Test;


public class TestPhoneNumberValidator {

    @Test
    public void withBrackets() {
        PhoneNumberValidator validator = new PhoneNumberValidator(true);

        assertEquals(true, validator.validate("5175433424")); 
        assertEquals(true, validator.validate("(517)5433424")); 
        assertEquals(false, validator.validate("(517)-543-3424")); 
        assertEquals(false, validator.validate("517-543-3424")); 
    }

    public void withoutBrackers(){
        PhoneNumberValidator validator = new PhoneNumberValidator(false);

        assertEquals(true, validator.validate("5175433424")); 
        assertEquals(false, validator.validate("(517)5433424")); 
        assertEquals(false, validator.validate("(517)-543-3424")); 
        assertEquals(false, validator.validate("517-543-3424")); 
    }
}
