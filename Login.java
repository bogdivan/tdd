public class Login {
    User[] users;
    public Login(User[] users) {
        this.users = users;
    }
    boolean validate(String username, String password, boolean admin) {
        for (User user : users) {
            if(
                user.getUsername().equals(username) &&
                user.getPassword().equals(password)){
                if(admin && user.getUser() == UserType.ADMIN){
                    return true;
                } else if(!admin){
                    return true;
                }
            }
        }
        return false;
    }
}