import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class TestPasswordGenerator {
    
    PasswordGenerator password = new PasswordGenerator(9);

    @Test
    public void testWeakPasswordGen(){
        assertEquals(9, password.generateWeakPassword().length());
    }

    @Test
    public void testStrongPasswordGen(){
        assertEquals(18, password.generateStrongPassword().length());
    }


}
